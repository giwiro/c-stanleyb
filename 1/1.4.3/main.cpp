#include <iostream>

using namespace std;

int main() {
    int i, sum = 0;
    while(cin >> i) {
        sum += i;
    }
    cout << "The sum is: " << sum << endl;
    return 0;
}
