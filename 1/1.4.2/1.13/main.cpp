#include <cstdio>
#include <algorithm>
#include <string>

main() {
    int sum = 0;
    for (int i = 51; i <= 100; ++i) {
        sum += i;
    }
    printf("The sum of the numbers from 50 to 100 is: %d\n", sum);
    printf("-------------------------\n");
    for (int j = 10; j > 0; --j) {
        printf("Number: %d\n", j);
    }
    printf("-------------------------\n");
    int a, b, max, i;
    printf("Inser 2 numbers\n");
    scanf("%d %d", &a, &b);
    max = std::max(a, b);
    i = std::min(a, b) + 1;
    for (; i < max; ++i) {
        printf("Number: %d\n", i);
    }
}


