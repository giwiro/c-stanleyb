#include <cstdio>
#include <algorithm>

main() {
    int a, b, max, i;
    printf("Insert 2 numbers\n");
    scanf("%d %d", &a, &b);
    max = std::max(a, b);
    i = std::min(a, b);
    while (i++, i != max) {
        printf("Number: %d\n", i);
    }
}
