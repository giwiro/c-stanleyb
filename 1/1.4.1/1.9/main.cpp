#include <cstdio>

main() {
    int i = 51, acum = 0;
    while(i <= 100) {
        acum = acum + i;
        ++i;
    }
    printf("The sum of numbers from 50 to 100 is: %d", acum);
}
